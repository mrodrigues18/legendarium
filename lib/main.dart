import 'package:flutter/material.dart';
import 'package:legendarium/fragments/admin_pages/author_pages/add_author.dart';
import 'package:legendarium/fragments/admin_pages/author_pages/add_book.dart';
import 'package:legendarium/fragments/admin_pages/author_pages/add_publisher.dart';
import 'package:legendarium/fragments/application_pages/forgotten.dart';
import 'package:legendarium/fragments/application_pages/login.dart';
import 'package:legendarium/fragments/application_pages/register.dart';
import 'package:legendarium/fragments/index_pages/view_index.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'fragments/application_pages/constants.dart';

Future main() async {
  SharedPreferences.setMockInitialValues({});

  WidgetsFlutterBinding.ensureInitialized();


  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        home: const MyHome(),
        routes: {
          'register': (context) => const MyRegister(),
          'login': (context) => const MyLogin(),
          'home': (context) => const MyHome(),
          'forgotten': (context) => const MyForgotten(),
          'add_author': (context) => const AddAuthorPage(),
          'add_book': (context) => const AddBookPage(),
          'add_publisher': (context) => const AddPublisherPage(),
        },
        theme: ThemeData(
          primaryColor: primaryColor,
        ),
      )
  );
}
