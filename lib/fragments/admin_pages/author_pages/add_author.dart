import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AddAuthorPage extends StatefulWidget {
  const AddAuthorPage({Key? key}) : super(key: key);

  @override
  _AddAuthorPageState createState() => _AddAuthorPageState();
}

class _AddAuthorPageState extends State<AddAuthorPage> {
  late Response response;
  var dio = Dio();

  TextEditingController textEditingControllerLastName  = TextEditingController();
  TextEditingController textEditingControllerFirstName = TextEditingController();
  TextEditingController textEditingControllerBiography = TextEditingController();
  String textErrorLastName = '';
  String textErrorFirstName = '';
  String textErrorBiography = '';
  String formValueLastName = '';
  String formValueFirstName = '';
  String formValueBiography = '';
  bool buttonDisabled = true;
  bool buttonPressed = false;
  bool errorLastNameEmailEnabled = false;
  bool errorFirstNameEmailEnabled = false;
  bool successInsert = false;


  Future<void> addAuthor(String authorLastName, String authorFirstName, String authorBiography) async {
    try {
      response = await dio.post(
          'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=addAuthorAndroid',
          options: Options(
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
          ),
          data: {
            'author_last_name': authorLastName,
            'author_first_name': authorFirstName,
            'author_biography': authorBiography
          });
      if(response.statusCode == 200) {
        if(jsonDecode(response.data) != null) {
          successInsert = true;
        }
        if(successInsert) {
          Navigator.pop(context);
        } else {
          setState(() {
            switch(jsonDecode(response.data)['result']['field']) {
              case 'lastName':
                errorLastNameEmailEnabled = true;
                textErrorLastName = jsonDecode(response.data)['result']['message'];
                break;
              case 'firstName':
                errorFirstNameEmailEnabled = true;
                textErrorFirstName = jsonDecode(response.data)['result']['message'];
                break;
            }
          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red, content: Text(jsonDecode(response.data)['result']['message'])));
        }
      }
    } on Exception catch (_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.redAccent, content: Text("Serveur éteint")));
    }
  }

  void checkFields() {
    buttonDisabled = false;
    if(formValueLastName.isEmpty || formValueFirstName.isEmpty) {
      buttonDisabled = true;
      checkAllFields();
    }
  }

  void checkAllFields() {
    if(formValueLastName.isNotEmpty && formValueFirstName.isNotEmpty) {
      setState(() {
        buttonPressed = true;
      });
      addAuthor(formValueLastName, formValueFirstName, formValueBiography);
    } else {
      setState(() {
        buttonPressed = false;
        if(formValueLastName.isEmpty) { errorLastNameEmailEnabled = true; textErrorLastName = "Veuillez vérifier l'information saisie"; }
        if(formValueFirstName.isEmpty) { errorFirstNameEmailEnabled = true; textErrorFirstName = "Veuillez vérifier l'information saisie"; }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35, top: 130),
              child: const Text(
                'Ajouter un auteur',
                style: TextStyle(fontSize: 33),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 35, right: 35),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: textEditingControllerFirstName,
                            onChanged: (inputValue) {
                              setState(() {
                                formValueFirstName = inputValue;
                                checkFields();
                              });
                            },
                            style: const TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                fillColor: Colors.grey.shade100,
                                filled: true,
                                errorText: errorFirstNameEmailEnabled ? textErrorFirstName : null,
                                hintText: "Prénom de l'auteur",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                            controller: textEditingControllerLastName,
                            onChanged: (inputValue) {
                              setState(() {
                                formValueLastName = inputValue;
                                checkFields();
                              });
                            },
                            style: const TextStyle(),
                            decoration: InputDecoration(
                              fillColor: Colors.grey.shade100,
                              filled: true,
                              errorText: errorLastNameEmailEnabled ? textErrorLastName : null,
                              hintText: "Nom de l'auteur",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                            controller: textEditingControllerBiography,
                            onChanged: (inputValue) {
                              setState(() {
                                formValueBiography = inputValue;
                                checkFields();
                              });
                            },
                            style: const TextStyle(),
                            decoration: InputDecoration(
                              fillColor: Colors.grey.shade100,
                              filled: true,
                              hintText: "Biographie",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                '',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w700),
                              ),
                              buttonDisabled ? CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff4c505b),
                                child: IconButton(
                                    splashColor: Colors.transparent,
                                    hoverColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    color: Colors.white54,
                                    onPressed: () {
                                      FocusScope.of(context).unfocus();
                                      checkAllFields();
                                    },
                                    icon: const Icon(
                                      Icons.arrow_forward,
                                    )
                                ),
                              ) : CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff4c505b),
                                child: IconButton(
                                    color: Colors.white,
                                    onPressed: ()  {
                                      FocusScope.of(context).unfocus();
                                      checkAllFields();
                                    },
                                    icon: const Icon(Icons.arrow_forward)
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
    );
  }
}
