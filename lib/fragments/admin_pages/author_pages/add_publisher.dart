import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AddPublisherPage extends StatefulWidget {
  const AddPublisherPage({Key? key}) : super(key: key);

  @override
  _AddPublisherPageState createState() => _AddPublisherPageState();
}

class _AddPublisherPageState extends State<AddPublisherPage> {
  late Response response;
  var dio = Dio();

  TextEditingController textEditingControllerLabel  = TextEditingController();
  String textErrorLabel = '';
  String formValueLabel = '';
  bool buttonDisabled = true;
  bool buttonPressed = false;
  bool errorLabelEnabled = false;
  bool successInsert = false;

  Future<void> addPublisher(String publisherLabel) async {
    try {
      response = await dio.post(
          'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=addPublisherAndroid',
          options: Options(
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
          ),
          data: {
            'publisher_label': publisherLabel
          });
      if(response.statusCode == 200) {
        if(jsonDecode(response.data) != null) {
          successInsert = true;
        }
        if(successInsert) {
          Navigator.pop(context);
        } else {
          setState(() {
            errorLabelEnabled = true;
            textErrorLabel = jsonDecode(response.data)['result']['message'];
          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red, content: Text(jsonDecode(response.data)['result']['message'])));
        }
      }
    } on Exception catch (_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.redAccent, content: Text("Serveur éteint")));
    }
  }

  void checkFields() {
    buttonDisabled = false;
    if(formValueLabel.isEmpty) {
      buttonDisabled = true;
      checkAllFields();
    }
  }

  void checkAllFields() {
    if(formValueLabel.isNotEmpty) {
      setState(() {
        buttonPressed = true;
      });
      addPublisher(formValueLabel);
    } else {
      setState(() {
        buttonPressed = false;
        if(formValueLabel.isEmpty) { errorLabelEnabled = true; textErrorLabel = "Veuillez vérifier l'information saisie"; }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 35, top: 130),
            child: const Text(
              'Ajouter un éditeur',
              style: TextStyle(fontSize: 33),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 35, right: 35),
                    child: Column(
                      children: [
                        TextFormField(
                          controller: textEditingControllerLabel,
                          onChanged: (inputValue) {
                            setState(() {
                              formValueLabel = inputValue;
                              checkFields();
                            });
                          },
                          style: const TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              fillColor: Colors.grey.shade100,
                              filled: true,
                              errorText: errorLabelEnabled ? textErrorLabel : null,
                              hintText: "Prénom de l'auteur",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )),
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              '',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w700),
                            ),
                            buttonDisabled ? CircleAvatar(
                              radius: 30,
                              backgroundColor: const Color(0xff4c505b),
                              child: IconButton(
                                  splashColor: Colors.transparent,
                                  hoverColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  color: Colors.white54,
                                  onPressed: () {
                                    FocusScope.of(context).unfocus();
                                    checkAllFields();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_forward,
                                  )
                              ),
                            ) : CircleAvatar(
                              radius: 30,
                              backgroundColor: const Color(0xff4c505b),
                              child: IconButton(
                                  color: Colors.white,
                                  onPressed: ()  {
                                    FocusScope.of(context).unfocus();
                                    checkAllFields();
                                  },
                                  icon: const Icon(Icons.arrow_forward)
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
