import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/constants.dart';
import 'package:http/http.dart' as http;

class AddBookPage extends StatefulWidget {
  const AddBookPage({Key? key}) : super(key: key);

  @override
  _AddBookPageState createState() => _AddBookPageState();
}

class _AddBookPageState extends State<AddBookPage> {
  late Response response;
  var dio = Dio();
  bool showFormEditBooks = false;
  List listOfAuthors = [];
  List listOfPublishers = [];
  List listOfAvailabilities = [];
  List listOfTypesBook = [];
  late String _formTitle;
  late String _formIsbn;
  late String _formSynopsis;
  late String _formPrice;
  late String _formQuantity;
  String _formTypeBook = "1";
  String _formAuthor = "1";
  String _formPublisher = "1";
  String _formAvailability = "1";
  String bookPicture = "book_no_cover.png";

  @override
  void initState() {
    super.initState();
    getAuthors();
    getPublishers();
    getAvailabilities();
    getTypesBook();
  }

  Future<void> getAuthors() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewAuthorsAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      listOfAuthors = resBody;
    });
  }

  Future<void> getPublishers() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewPublishersAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      listOfPublishers = resBody;
    });
  }

  Future<void> getAvailabilities() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewAvailabilitiesAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      listOfAvailabilities = resBody;
    });
  }

  Future<void> getTypesBook() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewTypesBookAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      listOfTypesBook = resBody;
    });
  }

  Future<void> saveBook(String bookTitle, String bookIsbn, String bookSynopsis, String bookPrice, String bookQuantity, String idAvailability, String idTypeBook, String idAuthor, String idPublisher, String idBook) async {
    response = await dio.post(
        'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=updateBookAndroid',
        options: Options(
          contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        ),
        data: {
          'book_title': bookTitle,
          'book_isbn': bookIsbn,
          'book_synopsis': bookSynopsis,
          'book_price': bookPrice,
          'book_quantity': bookQuantity,
          'book_picture': bookPicture,
          'id_availability': idAvailability,
          'id_type_book': idTypeBook,
          'id_author': idAuthor,
          'id_publisher': idPublisher,
          'book_id': idBook
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ajouter un livre"),
        backgroundColor: primaryColor,
      ),
      body: SingleChildScrollView(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width/1.5,
                                          child: TextFormField(
                                            onChanged: (inputValue) {
                                              setState(() {
                                                _formTitle = inputValue;
                                              });
                                            },
                                            style: const TextStyle(color: Colors.black),
                                            decoration: InputDecoration(
                                                fillColor: Colors.grey.shade100,
                                                filled: true,
                                                hintText: "Titre",
                                                border: InputBorder.none
                                            ),
                                          ),
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width/1.5,
                                          child: TextFormField(
                                            onChanged: (inputValue) {
                                              setState(() {
                                                _formIsbn = inputValue;
                                              });
                                            },
                                            style: const TextStyle(color: Colors.black),
                                            decoration: InputDecoration(
                                                fillColor: Colors.grey.shade100,
                                                filled: true,
                                                hintText: "ISBN",
                                                border: InputBorder.none
                                            ),
                                          ),
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width/1.5,
                                          child: TextFormField(
                                            maxLines: 10,
                                            onChanged: (inputValue) {
                                              setState(() {
                                                _formSynopsis = inputValue;
                                              });
                                            },
                                            style: const TextStyle(color: Colors.black),
                                            decoration: InputDecoration(
                                                fillColor: Colors.grey.shade100,
                                                filled: true,
                                                hintText: "Synopsis",
                                                border: InputBorder.none
                                            ),
                                          ),
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        DropdownButton(
                                          items: listOfAuthors.map((item) {
                                            return DropdownMenuItem(
                                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [Text(item['author_last_name']), const Text(" "), Text(item['author_first_name'])]),
                                              value: item['author_id'].toString(),
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              _formAuthor = newVal.toString();
                                            });
                                          },
                                          value: _formAuthor,
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        DropdownButton(
                                          items: listOfPublishers.map((item) {
                                            return DropdownMenuItem(
                                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [Text(item['publisher_label'])]),
                                              value: item['publisher_id'].toString(),
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              _formPublisher = newVal.toString();
                                            });
                                          },
                                          value: _formPublisher,
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        DropdownButton(
                                          style: const TextStyle(
                                              color: Colors.black
                                          ),
                                          items: listOfAvailabilities.map((item) {
                                            return DropdownMenuItem(
                                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [Text(item['availability_label'])]),
                                              value: item['availability_id'].toString(),
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              _formAvailability = newVal.toString();
                                            });
                                          },
                                          value: _formAvailability,
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        ElevatedButton(
                                          onPressed: () {

                                          },
                                          child: const Text("Choisir une image"),
                                          style: ElevatedButton.styleFrom(primary: primaryColor),
                                        ),
                                        const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                        DropdownButton(
                                          items: listOfTypesBook.map((item) {
                                            return DropdownMenuItem(
                                              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [Text(item['type_book_label'])]),
                                              value: item['type_book_id'].toString(),
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              _formTypeBook = newVal.toString();
                                            });
                                          },
                                          value: _formTypeBook,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    ElevatedButton(
                                        style: ElevatedButton.styleFrom(primary: primaryColor),
                                        onPressed: () {
                                        },
                                        child: const Text("Enregistrer")
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                      );
                    },
                  ),
                ),
              ]
          )
      ),
    );
  }
}
