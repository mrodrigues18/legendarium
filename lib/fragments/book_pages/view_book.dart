import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/constants.dart';
import 'package:legendarium/fragments/book_pages/webview_book.dart';
import 'package:legendarium/modele/ClusterBook.dart';
import 'package:legendarium/modele/User.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class ViewBook extends StatefulWidget {
  final ClusterBook book;
  final User? user;

  const ViewBook({Key? key, required this.book, required this.user}) : super(key: key);

  @override
  _ViewBookState createState() => _ViewBookState();
}

class _ViewBookState extends State<ViewBook> {
  late Response response;
  var dio = Dio();
  bool showFormEditBooks = false;
  List listOfAuthors = [];
  List listOfPublishers = [];
  List listOfAvailabilities = [];
  List listOfTypesBook = [];
  late String _formTitle;
  late String _formIsbn;
  late String _formSynopsis;
  late String _formPrice;
  late String _formQuantity;
  late String _formTypeBook;
  late String _formAuthor;
  late String _formPublisher;
  late String _formAvailability;
  String bookPicture = "book_no_cover.png";

  @override
  void initState() {
    super.initState();
    getAuthors();
    getPublishers();
    getAvailabilities();
    getTypesBook();
    _formTitle = widget.book.bookTitle;
    _formIsbn = widget.book.bookIsbn;
    _formSynopsis = widget.book.bookSynopsis!;
    _formPrice = widget.book.bookPrice;
    _formQuantity = widget.book.bookQuantity;
  }

  Future<void> getAuthors() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewAuthorsAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      _formAuthor = widget.book.idAuthor;
      listOfAuthors = resBody;
    });
  }

  Future<void> getPublishers() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewPublishersAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      _formPublisher = widget.book.idPublisher;
      listOfPublishers = resBody;
    });
  }

  Future<void> getAvailabilities() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewAvailabilitiesAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      _formAvailability = widget.book.idAvailability;
      listOfAvailabilities = resBody;
    });
  }

  Future<void> getTypesBook() async {
    const String url = "http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=viewTypesBookAndroid";
    var res = await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      _formTypeBook = widget.book.idTypeBook;
      listOfTypesBook = resBody;
    });
  }

  Future<void> saveBook(String bookTitle, String bookIsbn, String bookSynopsis, String bookPrice, String bookQuantity, String idAvailability, String idTypeBook, String idAuthor, String idPublisher, String idBook) async {
    response = await dio.post(
        'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=updateBookAndroid',
        options: Options(
          contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        ),
        data: {
          'book_title': bookTitle,
          'book_isbn': bookIsbn,
          'book_synopsis': bookSynopsis,
          'book_price': bookPrice,
          'book_quantity': bookQuantity,
          'book_picture': bookPicture,
          'id_availability': idAvailability,
          'id_type_book': idTypeBook,
          'id_author': idAuthor,
          'id_publisher': idPublisher,
          'book_id': idBook
        });
    //if(response.statusCode == 200) {
      setState(() {

      });
      //ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red, content: Text(jsonDecode(response.data)['result']['message'])));
    //}
  }

  _launchURL() async {
    var url = 'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=updateCoverBookWeb&idBook=' + widget.book.bookId;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          persistentFooterButtons: [
            Row(
              mainAxisAlignment: (widget.user != null ? MainAxisAlignment.center : MainAxisAlignment.spaceAround),
              children: <Widget>[
                IconButton(
                    icon: const Icon(Icons.home, color: primaryColor),
                    onPressed: () {
                      Navigator.popAndPushNamed(context, 'home', arguments: widget.user);
                    },
                    tooltip: "Accueil"
                ),
                (widget.user == null ? IconButton(
                    icon: const Icon(Icons.account_circle_rounded, color: primaryColor),
                    onPressed: () {
                      Navigator.popAndPushNamed(context, 'login');
                    },
                    tooltip: "Mon compte"
                ) :
                Container()
                )
              ],
            )
          ],
          appBar: AppBar(
            automaticallyImplyLeading: !showFormEditBooks ? true : false,
            actions: <Widget>[
              (widget.user != null && widget.user!.idRole == "1" ?
              Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        showFormEditBooks = !showFormEditBooks;
                        (!showFormEditBooks ?
                          saveBook(_formTitle, _formIsbn, _formSynopsis, _formPrice, _formQuantity, _formAvailability, _formTypeBook, _formAuthor, _formPublisher, widget.book.bookId) :
                          null
                        );
                        });
                    },
                    child: Icon(
                      (!showFormEditBooks ? Icons.edit : Icons.check_circle), color: secondaryColor,
                    ),
                  )
              ) : Container())
            ],
            backgroundColor: primaryColor,
          ),
          body: SingleChildScrollView(
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: ListView.builder(
                        itemCount: 1,
                        itemBuilder: (context, index) {
                          return Card(
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      (!showFormEditBooks ?
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget> [
                                          Image.network("http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/img/books/" + (widget.book.bookPicture!.isEmpty ? "book_no_cover.png" : widget.book.bookPicture.toString()), width: 148.0, height: 250.0, fit: BoxFit.cover,),
                                        ],
                                      ) :
                                      Container()
                                      ),
                                      (widget.user != null && widget.user!.idRole == "1" && showFormEditBooks ?
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: MediaQuery.of(context).size.width/2,
                                            child: TextFormField(
                                              initialValue: _formTitle,
                                              onChanged: (inputValue) {
                                                setState(() {
                                                  _formTitle = inputValue;
                                                });
                                              },
                                              style: const TextStyle(color: Colors.black),
                                              decoration: InputDecoration(
                                                  fillColor: Colors.grey.shade100,
                                                  filled: true,
                                                  hintText: "Titre",
                                                  border: InputBorder.none
                                              ),
                                            ),
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          SizedBox(
                                            width: MediaQuery.of(context).size.width/2,
                                            child: TextFormField(
                                              initialValue: _formIsbn,
                                              onChanged: (inputValue) {
                                                setState(() {
                                                  _formIsbn = inputValue;
                                                });
                                              },
                                              style: const TextStyle(color: Colors.black),
                                              decoration: InputDecoration(
                                                  fillColor: Colors.grey.shade100,
                                                  filled: true,
                                                  hintText: "ISBN",
                                                  border: InputBorder.none
                                              ),
                                            ),
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          SizedBox(
                                            width: MediaQuery.of(context).size.width/1.2,
                                            child: TextFormField(
                                              maxLines: 10,
                                              initialValue: _formSynopsis,
                                              onChanged: (inputValue) {
                                                setState(() {
                                                  _formSynopsis = inputValue;
                                                });
                                              },
                                              style: const TextStyle(color: Colors.black),
                                              decoration: InputDecoration(
                                                  fillColor: Colors.grey.shade100,
                                                  filled: true,
                                                  hintText: "Synopsis",
                                                  border: InputBorder.none
                                              ),
                                            ),
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          DropdownButton(
                                            items: listOfAuthors.map((item) {
                                              return DropdownMenuItem(
                                                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [Text(item['author_last_name']), const Text(" "), Text(item['author_first_name'])]),
                                                value: item['author_id'].toString(),
                                              );
                                            }).toList(),
                                            onChanged: (newVal) {
                                              setState(() {
                                                _formAuthor = newVal.toString();
                                              });
                                            },
                                            value: _formAuthor,
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          DropdownButton(
                                            items: listOfPublishers.map((item) {
                                              return DropdownMenuItem(
                                                child: Text(item['publisher_label']),
                                                value: item['publisher_id'].toString(),
                                              );
                                            }).toList(),
                                            onChanged: (newVal) {
                                              setState(() {
                                                _formPublisher = newVal.toString();
                                              });
                                            },
                                            value: _formPublisher,
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          DropdownButton(
                                            items: listOfAvailabilities.map((item) {
                                              return DropdownMenuItem(
                                                child: Text(item['availability_label']),
                                                value: item['availability_id'].toString(),
                                              );
                                            }).toList(),
                                            onChanged: (newVal) {
                                              setState(() {
                                                _formAvailability = newVal.toString();
                                              });
                                            },
                                            value: _formAvailability,
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          ElevatedButton(
                                            onPressed: () {
                                              _launchURL();
                                            },
                                            child: const Text("Choisir une image"),
                                            style: ElevatedButton.styleFrom(primary: primaryColor),
                                          ),
                                          const Padding(padding: EdgeInsets.only(bottom: 5.0)),
                                          DropdownButton(
                                            items: listOfTypesBook.map((item) {
                                              return DropdownMenuItem(
                                                child: Text(item['type_book_label']),
                                                value: item['type_book_id'].toString(),
                                              );
                                            }).toList(),
                                            onChanged: (newVal) {
                                              setState(() {
                                                _formTypeBook = newVal.toString();
                                              });
                                            },
                                            value: _formTypeBook,
                                          ),
                                        ],
                                      ) :
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(padding: const EdgeInsets.only(top: 10.0, left: 5.0), child: Text(widget.book.bookTitle, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))),
                                          Padding(padding: const EdgeInsets.only(top: 10.0, left: 5.0), child: Text(widget.book.authorLabel, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: Colors.blue),)),
                                          Padding(padding: const EdgeInsets.only(top: 10.0, left: 5.0), child: Text(widget.book.publisherLabel, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: Colors.blue),)),
                                        ],
                                      )
                                      )
                                    ],
                                  ),
                                  (!showFormEditBooks ?
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(primary: primaryColor),
                                          onPressed: () {
                                          },
                                          child: Text(widget.book.availabilityLabel)
                                      )
                                    ],
                                  ) :
                                  Container()
                                  )
                                ],
                              ),
                            )
                          );
                        },
                      ),
                    ),
                  ]
              )
          )
      );
  }
}
