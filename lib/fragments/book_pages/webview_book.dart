import 'dart:io';

import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/constants.dart';

class WebViewBook extends StatefulWidget {
  final String idBook;
  const WebViewBook({Key? key, required this.idBook}) : super(key: key);

  @override
  _WebViewBookState createState() => _WebViewBookState();
}

class _WebViewBookState extends State<WebViewBook> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mise à jour de la couverture'),
        backgroundColor: primaryColor,
      ),
      body: Container(),
    );
  }
}