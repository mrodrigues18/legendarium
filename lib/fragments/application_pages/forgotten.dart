import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/functions.dart';

class MyForgotten extends StatefulWidget {
  const MyForgotten({Key? key}) : super(key: key);

  @override
  _MyForgottenState createState() => _MyForgottenState();
}

class _MyForgottenState extends State<MyForgotten> {
  TextEditingController textEditingControllerEmail = TextEditingController();
  bool buttonDisabled = true;
  bool errorEnabled = false;
  String textError = '';

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/forgotten.png'), fit: BoxFit.cover),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              foregroundColor: Colors.black,
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            body: Stack(
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 35, top: 30),
                    child: const Text(
                      'Récupérer son compte',
                      style: TextStyle(color: Colors.white, fontSize: 33),
                    ),
                  ),
                  SingleChildScrollView(
                      child: Container(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.28),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin: const EdgeInsets.only(left: 35, right: 35),
                                    child: Column(
                                        children: [
                                          TextFormField(
                                            onChanged: (input) {
                                              setState(() {
                                                errorEnabled = false;
                                                if(input.isNotEmpty && !input.isValidEmail()) {
                                                  errorEnabled = true;
                                                  textError = "Adresse email incorrecte";
                                                }
                                                buttonDisabled = true;
                                                if(input.isValidEmail()) {
                                                  buttonDisabled = false;
                                                }
                                              });
                                              return;
                                            },
                                            autofocus: true,
                                            style: const TextStyle(
                                                color: Colors.white
                                            ),
                                            controller: textEditingControllerEmail,
                                            decoration: InputDecoration(
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(10),
                                                  borderSide: const BorderSide(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(10),
                                                  borderSide: const BorderSide(
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                prefixIcon: const Icon(Icons.account_circle_outlined, color: Colors.white),
                                                errorText: errorEnabled ? textError : null,
                                                hintText: "Nom d'utilisateur / Email",
                                                hintStyle: const TextStyle(
                                                    color: Colors.white
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(10),
                                                )
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 30,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              buttonDisabled ? ElevatedButton(
                                                  child: const Text(
                                                      "Récupérer mon mot de passe",
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w700,
                                                      )
                                                  ),
                                                  onPressed: () {
                                                  },
                                                  style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all<Color>(const Color.fromRGBO(187,215,182, 1))
                                                  )
                                              ) :
                                              ElevatedButton(
                                                  child: const Text(
                                                      "Récupérer mon mot de passe",
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w700,
                                                      )
                                                  ),
                                                  onPressed: () {
                                                  },
                                                  style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all<Color>(const Color.fromRGBO(120, 176, 109, 1.0))
                                                  )
                                              )
                                            ],
                                          ),
                                        ]
                                    )
                                )
                              ]
                          )
                      )
                  )
                ]
            )
        )
    );
  }
}