import 'dart:ui';

const Color primaryColor = Color.fromRGBO(36, 100, 94, 1);
const Color secondaryColor = Color.fromRGBO(119, 175, 108, 1);