import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session_manager/flutter_session_manager.dart';
import 'package:legendarium/modele/User.dart';

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  _MyLoginState createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {
  late Response response;
  var dio = Dio();
  var sessionManager = SessionManager();
  late User aUser;

  TextEditingController textEditingControllerNicknameEmail  = TextEditingController();
  TextEditingController textEditingControllerPassword       = TextEditingController();
  String textErrorNicknameEmail = '';
  String textErrorPassword = '';
  String formValueNicknameEmail = '';
  String formValuePassword = '';
  bool buttonDisabled = true;
  bool buttonPressed = false;
  bool errorNicknameEmailEnabled = false;
  bool errorPasswordEnabled = false;
  bool passwordVisible = false;
  bool successLogIn = false;

  void togglePassword() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }

  Future<void> connect(String userNicknameEmail, String userPassword) async {
    try {
      response = await dio.post(
          'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=loginAndroid',
          options: Options(
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
          ),
          data: {
            'user_nickname_email': userNicknameEmail,
            'user_password': userPassword
          });
      if(response.statusCode == 200) {
        if(jsonDecode(response.data)['aUser'] != null) {
          aUser = User.fromJson(jsonDecode(response.data)['aUser']);
          successLogIn = true;
        }
        if(successLogIn) {
          Navigator.popAndPushNamed(context, 'home', arguments: aUser);
        } else {
          setState(() {
            switch(jsonDecode(response.data)['result']['field']) {
              case 'login':
                errorNicknameEmailEnabled = true;
                textErrorNicknameEmail = jsonDecode(response.data)['result']['message'];
                break;
              case 'password':
                errorPasswordEnabled = true;
                textErrorPassword = jsonDecode(response.data)['result']['message'];
                break;
            }
          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red, content: Text(jsonDecode(response.data)['result']['message'])));
        }
      }
    } on Exception catch (_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.redAccent, content: Text("Serveur éteint")));
    }
  }

  void checkFields() {
    buttonDisabled = false;
    if(formValueNicknameEmail.isEmpty || formValuePassword.isEmpty) {
      buttonDisabled = true;
      checkAllFields();
    }
  }

  void checkAllFields() {
    if(formValueNicknameEmail.isNotEmpty && formValuePassword.isNotEmpty) {
      setState(() {
        buttonPressed = true;
      });
      connect(formValueNicknameEmail, formValuePassword);
    } else {
      setState(() {
        buttonPressed = false;
        if(formValueNicknameEmail.isEmpty) { errorNicknameEmailEnabled = true; textErrorNicknameEmail = "Veuillez vérifier l'information saisie: trop courte"; }
        if(formValuePassword.isEmpty) { errorPasswordEnabled = true; textErrorPassword = "Longueur < à 6"; }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/login.png'), fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35, top: 130),
              child: const Text(
                'Bienvenue',
                style: TextStyle(color: Colors.white, fontSize: 33),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 35, right: 35),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: textEditingControllerNicknameEmail,
                            onChanged: (inputValue) {
                              setState(() {
                                formValueNicknameEmail = inputValue;
                                if(inputValue.isNotEmpty && inputValue.length < 2) {
                                  errorNicknameEmailEnabled = true;
                                  textErrorNicknameEmail = "Veuillez vérifier l'information saisie: trop courte";
                                  formValueNicknameEmail = "";
                                } else {
                                  errorNicknameEmailEnabled = false;
                                  textErrorNicknameEmail = "";
                                }
                                checkFields();
                              });
                            },
                            style: const TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.account_circle_outlined),
                                fillColor: Colors.grey.shade100,
                                filled: true,
                                errorText: errorNicknameEmailEnabled ? textErrorNicknameEmail : null,
                                hintText: "Nom d'utilisateur / Email",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                            controller: textEditingControllerPassword,
                            onChanged: (inputValue) {
                              setState(() {
                                formValuePassword = inputValue;
                                if(inputValue.isNotEmpty && inputValue.length < 6) {
                                  errorPasswordEnabled = true;
                                  textErrorPassword = "Mot de passe trop court: < 6";
                                  formValuePassword = "";
                                } else {
                                  errorPasswordEnabled = false;
                                  textErrorPassword = "";
                                }
                                checkFields();
                              });
                            },
                            style: const TextStyle(),
                            obscureText: passwordVisible ? false : true,
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.lock_outline),
                                suffixIcon: IconButton(
                                  icon: Icon(passwordVisible ? Icons.visibility_outlined : Icons.visibility_off_outlined),
                                  onPressed: () => togglePassword(),
                                  tooltip: "Afficher le mot de passe",
                                  mouseCursor: SystemMouseCursors.click,
                                  splashColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                ),
                                fillColor: Colors.grey.shade100,
                                filled: true,
                                errorText: errorPasswordEnabled ? textErrorPassword : null,
                                hintText: "Mot de passe",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                'Connexion',
                                style: TextStyle(
                                    fontSize: 27, fontWeight: FontWeight.w700),
                              ),
                              buttonDisabled ? CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff4c505b),
                                child: IconButton(
                                  splashColor: Colors.transparent,
                                  hoverColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  color: Colors.white54,
                                  onPressed: () {
                                    FocusScope.of(context).unfocus();
                                    checkAllFields();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_forward,
                                  )
                                ),
                              ) : CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff4c505b),
                                child: IconButton(
                                  color: Colors.white,
                                  onPressed: ()  {
                                    FocusScope.of(context).unfocus();
                                    checkAllFields();
                                  },
                                  icon: const Icon(Icons.arrow_forward)
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.pushNamed(context, 'register');
                                },
                                child: const Text(
                                  'Créer un compte',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Color(0xff4c505b),
                                      fontSize: 18),
                                ),
                                style: const ButtonStyle(),
                              ),
                              TextButton(
                                  onPressed: () => Navigator.pushNamed(context, 'forgotten'),
                                  child: const Text(
                                    'Mot de passe oublié',
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Color(0xff4c505b),
                                      fontSize: 18,
                                    ),
                                  )
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
