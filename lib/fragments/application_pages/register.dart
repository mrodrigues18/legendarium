import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/functions.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  _MyRegisterState createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  late Response response;
  var dio = Dio();

  TextEditingController textEditingControllerFirstName  = TextEditingController();
  TextEditingController textEditingControllerLastName   = TextEditingController();
  TextEditingController textEditingControllerNickname   = TextEditingController();
  TextEditingController textEditingControllerPhone      = TextEditingController();
  TextEditingController textEditingControllerEmail      = TextEditingController();
  TextEditingController textEditingControllerPassword   = TextEditingController();
  String formValueFirstName = '';
  String formValueLastName = '';
  String formValueNickname = '';
  String formValuePhone = '';
  String formValueEmail = '';
  String formValuePassword = '';
  bool errorFirstNameEnabled = false;
  bool errorLastNameEnabled = false;
  bool errorNicknameEnabled = false;
  bool errorPhoneEnabled = false;
  bool errorEmailEnabled = false;
  bool errorPasswordEnabled = false;
  String textErrorFirstName = '';
  String textErrorLastName = '';
  String textErrorNickname = '';
  String textErrorPhone = '';
  String textErrorEmail = '';
  String textErrorPassword = '';
  bool buttonDisabled = true;
  bool passwordVisible = false;

  bool successSignIn = false;

  void togglePassword() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }

  Future<void> insertUser(String userEmail, String userLastName, String userFirstName, String userNickname, String userPhone, String userPassword) async {
    response = await dio.post(
        'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=signinAndroid',
        options: Options(
          contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        ),
        data: {
          'user_email': userEmail,
          'user_last_name': userLastName,
          'user_first_name': userFirstName,
          'user_nickname': userNickname,
          'user_phone': userPhone,
          'user_password': userPassword
        });
    if(response.statusCode == 200) successSignIn = true;
  }

  void checkFields() {
    buttonDisabled = false;
    if(formValueFirstName.isEmpty || formValueLastName.isEmpty || formValueNickname.isEmpty || formValuePhone.isEmpty || formValueEmail.isEmpty || formValuePassword.isEmpty) {
      buttonDisabled = true;
      checkAllFields();
    }
  }

  void checkAllFields() {
    if(formValueFirstName.isNotEmpty && formValueLastName.isNotEmpty && formValueNickname.isNotEmpty && formValuePhone.isNotEmpty && formValueEmail.isNotEmpty && formValuePassword.isNotEmpty) {
      insertUser(formValueEmail, formValueLastName, formValueFirstName, formValueNickname, formValuePhone, formValuePassword);
    } else {
      setState(() {
        if(formValueFirstName.isEmpty) { errorFirstNameEnabled = true; textErrorFirstName = "Prénom trop court < 2"; }
        if(formValueLastName.isEmpty) { errorLastNameEnabled = true; textErrorLastName = "Nom trop court < 2"; }
        if(formValueNickname.isEmpty) { errorNicknameEnabled = true; textErrorNickname = "Nom d'utilisateur trop court < 2"; }
        if(formValuePhone.isEmpty) { errorPhoneEnabled = true; textErrorPhone = "Format du numéro incorrect"; }
        if(formValueEmail.isEmpty) { errorEmailEnabled = true; textErrorEmail = "Veuillez vérifier votre email"; }
        if(formValuePassword.isEmpty) { errorPasswordEnabled = true; textErrorPassword = "Longueur < à 6"; }
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/register.png'), fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 1,
        ),
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35, top: 30),
              child: const Text(
                'Créer un compte',
                style: TextStyle(color: Colors.white, fontSize: 33),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 35, right: 35),
                      child: Column(
                        children: [
                          TextField(
                            controller: textEditingControllerFirstName,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.length >= 2) {
                                formValueFirstName = inputValue;
                              }
                              if(inputValue.isNotEmpty && inputValue.length < 2) {
                                errorFirstNameEnabled = true;
                                textErrorFirstName = "Prénom trop court: < 2";
                                formValueFirstName = "";
                              } else {
                                errorFirstNameEnabled = false;
                                textErrorFirstName = "";
                              }
                              checkFields();
                            }),
                            textCapitalization: TextCapitalization.words,
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              errorText: errorFirstNameEnabled ? textErrorFirstName : null,
                              hintText: "Prénom",
                              hintStyle: const TextStyle(color: Colors.white54),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextField(
                            controller: textEditingControllerLastName,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.length >= 2) {
                                formValueLastName = inputValue;
                              }
                              if(inputValue.isNotEmpty && inputValue.length < 2) {
                                errorLastNameEnabled = true;
                                textErrorLastName = "Nom trop court: < 2";
                                formValueLastName = "";
                              } else {
                                errorLastNameEnabled = false;
                                textErrorLastName = "";
                              }
                              checkFields();
                            }),
                            textCapitalization: TextCapitalization.characters,
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              errorText: errorLastNameEnabled ? textErrorLastName : null,
                              hintText: "NOM",
                              hintStyle: const TextStyle(color: Colors.white54),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextField(
                            controller: textEditingControllerNickname,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.length >= 2) {
                                formValueNickname = inputValue;
                              }
                              if(inputValue.isNotEmpty && inputValue.length < 2) {
                                errorNicknameEnabled = true;
                                textErrorNickname = "Pseudonyme trop court: < 2";
                                formValueNickname = "";
                              } else {
                                errorNicknameEnabled = false;
                                textErrorNickname = "";
                              }
                              checkFields();
                            }),
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.account_circle_rounded),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              errorText: errorLastNameEnabled ? textErrorLastName : null,
                              hintText: "Nom d'utilisateur",
                              hintStyle: const TextStyle(color: Colors.white54),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextField(
                            controller: textEditingControllerPhone,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.isValidPhone()) {
                                formValuePhone = inputValue;
                              }
                              if(inputValue.isNotEmpty && !inputValue.isValidPhone()) {
                                errorPhoneEnabled = true;
                                textErrorPhone = "Format du numéro incorrect";
                                formValuePhone = "";
                              } else {
                                errorPhoneEnabled = false;
                                textErrorPhone = "";
                              }
                              checkFields();
                            }),
                            keyboardType: TextInputType.phone,
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.phone),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                    color: Colors.white,
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                    color: Colors.black,
                                  ),
                                ),
                                errorText: errorPhoneEnabled ? textErrorPhone : null,
                                hintText: "06 12 34 56 78",
                                hintStyle: const TextStyle(color: Colors.white54),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextField(
                            controller: textEditingControllerEmail,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.isValidEmail()) {
                                formValueEmail = inputValue;
                              }
                              if(inputValue.isNotEmpty && !inputValue.isValidEmail()) {
                                errorEmailEnabled = true;
                                textErrorEmail = "Format de l'adresse mail incorrect";
                                formValueEmail = "";
                              } else {
                                errorEmailEnabled = false;
                                textErrorEmail = "";
                              }
                              checkFields();
                            }),
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.mail_rounded),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              errorText: errorEmailEnabled ? textErrorEmail : null,
                              hintText: "Email",
                              hintStyle: const TextStyle(color: Colors.white54),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextField(
                            controller: textEditingControllerPassword,
                            onChanged: (inputValue) => setState(() {
                              if(inputValue.length >= 6) {
                                formValuePassword = inputValue;
                              }
                              if(inputValue.isNotEmpty && inputValue.length < 6) {
                                errorPasswordEnabled = true;
                                textErrorPassword = "Mot de passe trop court";
                                formValuePassword = "";
                              } else {
                                errorPasswordEnabled = false;
                                textErrorPassword = "";
                              }
                              checkFields();
                            }),
                            style: const TextStyle(color: Colors.white),
                            obscureText: passwordVisible ? false : true,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.lock),
                              suffixIcon: IconButton(
                                icon: Icon(passwordVisible ? Icons.visibility_outlined : Icons.visibility_off_outlined),
                                onPressed: () => togglePassword(),
                                tooltip: "Afficher le mot de passe",
                                mouseCursor: SystemMouseCursors.click,
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                              errorText: errorPasswordEnabled ? textErrorPassword : null,
                              hintText: "Mot de passe",
                              hintStyle: const TextStyle(color: Colors.white54),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              )
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                'Créer un compte',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 27,
                                    fontWeight: FontWeight.w700),
                              ),
                              buttonDisabled ? CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff737373),
                                child: IconButton(
                                  splashColor: Colors.transparent,
                                  hoverColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                    color: Colors.white54,
                                    onPressed: () {
                                      checkAllFields();
                                    },
                                    icon: const Icon(
                                      Icons.arrow_forward,
                                    )),
                              ) :
                              CircleAvatar(
                                radius: 30,
                                backgroundColor: const Color(0xff4c505b),
                                child: IconButton(
                                    color: Colors.white,
                                    onPressed: () {
                                      checkAllFields();
                                      if(successSignIn) {
                                        FocusScope.of(context).unfocus();
                                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Vous pouvez maintenant vous connecter"), backgroundColor: Colors.green,));
                                        Navigator.pushNamed(context, 'login');
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.arrow_forward,
                                    )),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.pushNamed(context, 'login');
                                },
                                child: const Text(
                                  'Se connecter',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Colors.white,
                                      fontSize: 18),
                                ),
                                style: const ButtonStyle(),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
