import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:legendarium/fragments/application_pages/constants.dart';
import 'package:legendarium/fragments/application_pages/login.dart';
import 'package:legendarium/fragments/book_pages/view_book.dart';
import 'package:legendarium/modele/ClusterBook.dart';
import 'package:legendarium/modele/User.dart';
import 'package:shimmer/shimmer.dart';
import '../../modele/Book.dart';

class MyHome extends StatefulWidget {
  final User? user;

  const MyHome({Key? key, this.user}) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  late Response response;
  var dio = Dio();
  late List<ClusterBook> clusterBooksList;
  bool showFormBooks = false;
  List<Book>? booksList;

  Future<List<ClusterBook>> futureClusterBooksList() async {
    try {
      response = await dio.get(
          'http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/?page=indexAndroid',
          options: Options(
            contentType: "charset=UTF-8",
          )
      );
      if(response.statusCode == 200) {
        final items = json.decode(response.data).cast<Map<String, dynamic>>();
          clusterBooksList = items.map<ClusterBook>((json) {
            return ClusterBook.fromJson(json);
          }).toList();
          return clusterBooksList;
      } throw Exception('Une erreur s\'est produite');
    } on Exception catch (_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.redAccent, content: Text("Serveur éteint")));
      return clusterBooksList;
    }
  }

  void selectedItem(BuildContext context, item) {
    switch (item) {
      case 0:
        Navigator.pushNamed(context, 'add_author');
        break;
      case 1:
        Navigator.pushNamed(context, 'add_publisher');
        break;
      case 2:
        Navigator.pushNamed(context, 'add_book');
        break;
      case 3:
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
            builder: (context) => const MyLogin()), (route) => false
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    var user;
    if(ModalRoute.of(context)!.settings.arguments != null) {
      user = ModalRoute.of(context)!.settings.arguments as User;
    }

    return Scaffold(
      persistentFooterButtons: [
        Row(
          mainAxisAlignment: (user != null ? MainAxisAlignment.center : MainAxisAlignment.spaceAround),
          children: <Widget>[
            IconButton(
                icon: const Icon(Icons.home, color: primaryColor),
                onPressed: () {
                  Navigator.popAndPushNamed(context, 'home', arguments: user);
                },
                tooltip: "Accueil"
            ),
            (user == null ? IconButton(
                icon: const Icon(Icons.account_circle_rounded, color: primaryColor),
                onPressed: () {
                  Navigator.popAndPushNamed(context, 'login');
                },
                tooltip: "Mon compte"
            ) :
            Container()
            )
          ],
        )
      ],
      appBar: AppBar(
        automaticallyImplyLeading: !showFormBooks ? true : false,
        actions: <Widget>[
          //(user != null && user.idRole == "1" ?
          Theme(
            data: Theme.of(context).copyWith(
                textTheme: const TextTheme().apply(bodyColor: Colors.black),
                dividerColor: Colors.white,
                iconTheme: const IconThemeData(color: Colors.white)),
            child: PopupMenuButton<int>(
              color: Colors.black,
              itemBuilder: (context) => [
                const PopupMenuItem<int>(
                    value: 0,
                    child: Text("Ajouter un auteur")
                ),
                const PopupMenuItem<int>(
                    value: 1,
                    child: Text("Ajouter un éditeur")
                ),
                const PopupMenuItem<int>(
                    value: 2,
                    child: Text("Ajouter un livre")
                ),
                const PopupMenuDivider(),
                PopupMenuItem<int>(
                    value: 3,
                    child: Row(
                      children: const [
                        Icon(
                          Icons.logout,
                          color: Colors.red,
                        ),
                        SizedBox(
                          width: 7,
                        ),
                        Text("Logout")
                      ],
                    )),
              ],
              onSelected: (item) => selectedItem(context, item),
            ),
          )
        //: Container()),
        ],
        backgroundColor: primaryColor,
        title: user != null ? Row(children: <Widget>[const Text("Bienvenue "), Text(user.userNickname.toString(), style: const TextStyle(fontWeight: FontWeight.bold, color: secondaryColor),)],) : Text("Accueil"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Padding(
                padding: EdgeInsets.all(10.0)
            ),
            SizedBox(
              height: 40.0,
              child: ListView.builder(
                physics: const ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return Row(
                    children: <Widget>[
                      ElevatedButton(
                        onPressed: () {  },
                        child: const Text('Filtre', textAlign: TextAlign.center),
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          shape: const StadiumBorder(),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(5.0))
                    ],
                  );
                },
              ),
            ),
            Container(padding: const EdgeInsets.only(top: 20.0, bottom: 5.0, left: 15.0), alignment: Alignment.centerLeft, child: const Text(
              'Les coups de coeurs',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            ),
            SizedBox(
              height: 400.0,
              child: FutureBuilder <List<ClusterBook>>(
                future: futureClusterBooksList(),
                builder: (context, snapshot) {
                  if(!snapshot.hasData) {
                    return ListView(
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      children: <Widget>[
                        Center(
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            child: Row(
                              children: <int>[0, 1, 2].map((_) => Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            width: 148.0,
                                            height: 250.0,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 1.0),
                                    ),
                                    Container(
                                      width: 50.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )).toList(),
                            ),
                          ),
                        )
                      ],
                    );
                  }
                  return
                    ListView(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      physics: const ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: snapshot.data!.map((data) => Card(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ViewBook(book: data, user: user)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget> [
                              Image.network("http://serveur1.arras-sio.com/symfony4-4017/legendarium/web/img/books/" + data.bookPicture.toString(), width: 148.0, height: 250.0, fit: BoxFit.cover,),
                              Padding(
                                  padding: const EdgeInsets.only(left: 5.0, bottom: 5.0, right: 5.0, top: 20.0),
                                  child: Row(
                                      children: <Widget>[
                                        Icon(Icons.radio_button_checked, size: 12.0, color: (
                                            data.idAvailability == "1" ? Colors.green :
                                            data.idAvailability == "2" ? Colors.orange :
                                            data.idAvailability == "3" ? Colors.red :
                                            data.idAvailability == "4" ? Colors.blue :
                                            Colors.black
                                        )
                                        ),
                                        Text(" " + data.availabilityLabel,
                                          style: TextStyle(
                                              color: (
                                                  data.idAvailability == "1" ? Colors.green :
                                                  data.idAvailability == "2" ? Colors.orange :
                                                  data.idAvailability == "3" ? Colors.red :
                                                  data.idAvailability == "4" ? Colors.blue :
                                                  Colors.black
                                              )
                                          ),)
                                      ]
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Row(
                                      children: [
                                        Text(data.bookTitle.substring(0, (data.bookTitle.length < 14 ? data.bookTitle.length : 14)) + (data.bookTitle.length > 14 ? "..." : ""))
                                      ]
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Row(
                                      children: [
                                        Text(data.authorLabel),
                                      ]
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Row(
                                      children: <Widget>[
                                        Text("4,3 "),
                                        const Icon(Icons.star, color: primaryColor, size: 12.0,),
                                        Text(" " + data.bookPrice + " €", style: const TextStyle(color: Colors.green),)
                                      ]
                                  )
                              ),
                            ],
                          ),
                        )
                      )
                    ).toList()
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const <Widget>[
                    Text(
                      'Demo Headline 2',
                      style: TextStyle(fontSize: 18),
                    ),
                    Icon(Icons.arrow_right_alt, color: primaryColor),
                  ],
                ),
              ),
              onTap: () {
                print('Motivation ${int}');
              },
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height/3,
              width: MediaQuery.of(context).size.width,
              child: FutureBuilder <List<ClusterBook>>(
                future: futureClusterBooksList(),
                builder: (context, snapshot) {
                  if(!snapshot.hasData) {
                    return ListView(
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                      children: <Widget>[
                        Center(
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            child: Row(
                              children: <int>[0, 1, 2].map((_) => Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            width: 148.0,
                                            height: 250.0,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 1.0),
                                    ),
                                    Container(
                                      width: 50.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )).toList(),
                            ),
                          ),
                        )
                      ],
                    );
                  }
                  return
                    ListView(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        children: snapshot.data!.map((data) => Card(
                          child: ListTile(title: Text('Motivation $int'), subtitle: Text('this is a description of the motivation')),
                        )).toList()
                    );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}