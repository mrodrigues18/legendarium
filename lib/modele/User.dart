class User {
  String userId;
  String userEmail;
  String userLastName;
  String userFirstName;
  String userNickname;
  String userPhone;
  String? userPicture;
  String idRole;



  User({required this.userId, required this.userEmail, required this.userLastName, required this.userFirstName,	required this.userNickname,	required this.userPhone, this.userPicture, required this.idRole});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        userId: json['user_id'],
        userEmail: json['user_email'],
        userLastName: json['user_last_name'],
        userFirstName: json['user_first_name'],
        userNickname: json['user_nickname'],
        userPhone: json['user_phone'],
        userPicture: json['user_picture'],
        idRole: json['id_role']
    );
  }
}