class Author {
  String authorId;
  String authorLastName;
  String authorFirstName;
  String? authorBiography;

  Author({
    required this.authorId,
    required this.authorLastName,
    required this.authorFirstName,
    this.authorBiography
  });

  factory Author.fromJson(Map<String, dynamic> json) {
    return Author(
      authorId: json['author_id'],
      authorLastName: json['author_last_name'],
      authorFirstName: json['author_first_name'],
      authorBiography: json['author_biography'],
    );
  }
}