class Book {
  String bookId;
  String bookTitle;
  String bookIsbn;
  String? bookSynopsis;
  String bookPrice;
  String bookQuantity;
  String? bookPicture;
  String idAvailability;
  String availabilityLabel;
  String idTypeBook;
  String typeBookLabel;
  String idAuthor;
  String authorLabel;
  String idPublisher;
  String publisherLabel;

  Book({
    required this.bookId,
    required this.bookTitle,
    required this.bookIsbn,
    this.bookSynopsis,
    required this.bookPrice,
    required this.bookQuantity,
    this.bookPicture,
    required this.idAvailability,
    required this.idTypeBook,
    required this.idAuthor,
    required this.idPublisher,
    required this.availabilityLabel,
    required this.typeBookLabel,
    required this.authorLabel,
    required this.publisherLabel
  });

  factory Book.fromJson(Map<String, dynamic> json) {
    return Book(
        bookId: json['book_id'],
        bookTitle: json['book_title'],
        bookIsbn: json['book_isbn'],
        bookSynopsis: json['book_synopsis'],
        bookPrice: json['book_price'],
        bookQuantity: json['book_quantity'],
        bookPicture: json['book_picture'],
        idAvailability: json['id_availability'],
        availabilityLabel: json['availability_label'],
        idTypeBook: json['id_type_book'],
        typeBookLabel: json['type_book_label'],
        idAuthor: json['id_author'],
        authorLabel: json['author_label'],
        idPublisher: json['id_publisher'],
        publisherLabel: json['publisher_label'],
    );
  }
}